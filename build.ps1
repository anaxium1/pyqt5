# Set Visual Studio Variables
pushd 'C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build'
cmd /c "vcvarsall.bat x86 & set" |
foreach {
  if ($_ -match "=") {
    $v = $_.split("="); set-item -force -path "ENV:\$($v[0])"  -value "$($v[1])"
  }
}
popd

$toppath = Get-Location

mkdir qt
cd qt
$qtpath = Get-Location
curl.exe -L -o qt.zip https://gitlab.com/anaxium1/qt5/-/jobs/artifacts/master/raw/qt.zip?job=job
7z x qt.zip
rm qt.zip

cd $toppath
mkdir python
cd python
$pythonpath = Get-Location
Copy-Item -Path "C:\Python36-32\*" -Destination $pythonpath -Recurse
curl.exe -L -o sip.zip https://gitlab.com/anaxium1/sip/-/jobs/artifacts/master/raw/sip.zip?job=job
7z x sip.zip
$env:Path = "$pythonpath;$env:Path"

cd $toppath
mkdir out
cd out
$outpath = Get-Location

cd $toppath
curl.exe -L -o PyQt5-5.15.2.tar.gz https://files.pythonhosted.org/packages/source/P/PyQt5/PyQt5-5.15.2.tar.gz
7z x -y PyQt5-5.15.2.tar.gz
7z x -y PyQt5-5.15.2.tar
cd PyQt5-5.15.2
python configure.py --confirm-license --qmake "$qtpath\bin\qmake.exe" --no-designer-plugin --no-qml-plugin --no-tools -b $outpath -d "$outpath\Lib\site-packages" -v "$outpath\sip\PyQt5"
nmake
nmake install

cd $outpath
7z a -r ..\pyqt5.zip *
